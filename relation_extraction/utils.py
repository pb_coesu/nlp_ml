import json
from json import JSONEncoder, JSONDecoder
import re
import ast

import pandas as pd

from tkdatabase.engine import ArticleEngine as AE
from tkdatabase.article import Stock, Sector_Topic_Keywords, Article, ArticleRaw, Property, PropertyKeyword
from regex_patterns import *

class PI_Encoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj,Position_Info):
            new_attr={str(key):val for key,val in obj.attr.items()}
            return self.encode({'locate':obj.locate,
                                'code':obj.code,
                                'name':obj.name,
                                'attr':new_attr,
                                'alias':obj.alias,
                                'is_alias':obj.is_alias,
                                'is_right':obj.is_right,
                                'text':obj.text
                                })

def pi_result_decoder(result_json):
    '''
    将通过PI_Encoder储存的json
    :param result_json: 通过PI_Encoder储存的json文件读取获得的数据
    :return:格式化好的对象，list(multi* Position_Info)
    '''
    for ind, val in enumerate(result_json):
        val = json.loads(val)
        tmp_obj = Position_Info(val["locate"], val["code"], val['name'], val['alias'], val['is_alias'])
        tmp_obj.locate = tuple(tmp_obj.locate)
        tmp_obj.is_right = val['is_right']
        tmp_obj.text = val['text']
        tmp_obj.attr = {ast.literal_eval(k): v for k, v in val['attr'].items()}
        result_json[ind] = tmp_obj
    return result_json

class Position_Info:
    # 记录某个位置的信息，包括股票代码、股票名称、分离属性、对齐的alias
    def __init__(self,locate,code,name,alias,is_alias):
        self.locate=locate
        self.code=code
        self.name=name
        # attr为attr位置：attr名字
        self.attr=dict()
        self.alias=alias
        # is_alias为1即通过alias识别的实体
        self.is_alias=is_alias
        # is_right为1即识别实体正确，0为错误，标注用。
        self.is_right=1
        self.text=''

    def __str__(self):
        if self.alias is None:
            alias='None'
        else:
            alias=self.alias
        return '    '.join([str(self.locate),self.code,self.name, str(self.attr), self.text])

def cal_total_location(aliasNameLocation, totalNameLocation):
    '''
    双指针方式进行实体对齐
    :param aliasNameLocation: re span 函数的实体别名返回结果，元组的第一个元素是关键字在文章中的起始位置，第二个元素是关键字在文章中的结束位置
    :param totalNameLocation: re span 函数的实体全名返回结果，元组的第一个元素是关键字在文章中的起始位置，第二个元素是关键字在文章中的结束位置
    :return-> list:
    '''
    if not aliasNameLocation:
        return totalNameLocation
    if not totalNameLocation:
        return aliasNameLocation
    pointTotalName = 0
    pointAliasName = 0
    total_location = []
    while True:
        if aliasNameLocation[pointAliasName].locate[1] + 5 < totalNameLocation[pointTotalName].locate[0]:
            # a<b a录库
            total_location.append(aliasNameLocation[pointAliasName])
            pointAliasName += 1
        elif totalNameLocation[pointTotalName].locate[1] + 5 < aliasNameLocation[pointAliasName].locate[0]:
            # a>b b录库
            total_location.append(totalNameLocation[pointTotalName])
            pointTotalName += 1
        else:
            # 落入区间，b录库
            total_location.append(totalNameLocation[pointTotalName])
            pointAliasName += 1
            pointTotalName += 1

        # 一方越界，剩余一方全入库
        if pointAliasName == len(aliasNameLocation):
            total_location += totalNameLocation[pointTotalName:]
            break
        if pointTotalName == len(totalNameLocation):
            total_location += aliasNameLocation[pointAliasName:]
            break
    return total_location

def get_stock_locations(stock_names, artical):
    '''
    在文章中找到所有命名实体的位置（关键词法），并进行实体对齐（固定位置法）
    '''
    all_stock_locations = []
    for stock in stock_names:
        # *ST是指连续亏损3年，ST是连续亏损2年
        totalName = stock[1].replace('*', '')
        totalNameLocation = [Position_Info(m.span(),stock[0],stock[1],stock[2],0) for m in re.finditer(totalName, artical)]
        if stock[2] is not None:
            aliasNameLocation = [Position_Info(m.span(),stock[0],stock[1],stock[2],1) for m in re.finditer(stock[2], artical)]
        else:
            aliasNameLocation = None
        kmp_result = cal_total_location(aliasNameLocation, totalNameLocation)
        if kmp_result:
            all_stock_locations += kmp_result
    combine_stock_locations = []
    for i in all_stock_locations:
        combine_stock_locations.append(i.locate)
    return all_stock_locations, combine_stock_locations

def get_attr_belong_location(combine_tmp_locations, attr_location, attr):
    '''
    某个属性对应的实体的re span位置
    实体与属性的位置进行合并并排序，假设属性之前的第一个实体为此属性对应的实体
    如果属性之前无实体，则返回None
    :param combine_tmp_locations:实体和属性位置的合并列表
    :param attr_location:属性的位置列表
    :param attr:当前要查找的属性所在位置
    :return:找到实体的re span位置
    '''
    n = combine_tmp_locations.index(attr)
    while n != 0:
        n -= 1
        if combine_tmp_locations[n] not in attr_location:
            return combine_tmp_locations[n]
    return None

def get_attribute_locations(attributes, stocks, artical):
    '''
    获得一篇文章中实体的属性，主要函数
    :param attributes:属性名列表
    :param stocks:实体名列表
    :param artical:文章
    :return:dict(股票id:set(属性值))
    '''
    stock_locations, combine_stock_locations = get_stock_locations(stocks, artical)
    for attr in attributes:
        # 每个attr是属性的字符串
        attr_location = [m.span() for m in re.finditer(attr, artical)]
        combine_tmp_locations = combine_stock_locations + attr_location
        combine_tmp_locations.sort(key=lambda x: x[0])
        for each_attr in attr_location:
            # 每个each_attr是一个location

            # 解决属性与股票重名问题Begin
            # 比如股票叫“XX国际”，属性为“国际”
            record_flag=False
            for stock_locate in combine_stock_locations:
                if each_attr[0]>=stock_locate[0] and each_attr[0]<=stock_locate[1]:
                    record_flag=True
                    break
            if record_flag:
                continue
            # 解决属性与股票重名问题OVER

            attr_belong_location = get_attr_belong_location(combine_tmp_locations, attr_location, each_attr)
            if attr_belong_location is None:
                # pass
                continue
            for pi in stock_locations:
                if attr_belong_location == pi.locate:
                    pi.attr[each_attr]=[attr,1]
    stock_locations = [i for i in stock_locations if i.attr]
    return stock_locations

def get_property_info():
    '''
    获得属性的keyword
    :return: 属性唯一，返回字典，属性：list，list为keyword list
    '''
    with AE().session_scope() as s:
        pk=s.query(PropertyKeyword).all()
        ppt=s.query(Property).all()
    pk_=pd.DataFrame([[i.property_id,i.keyword] for i in pk])
    return {i.property:list(pk_[pk_[0]==i.property_id][1]) for i in ppt}

def get_text(artical, end_locate, result, b=2, e=1):
    '''
    获得股票所在位置的前b句话及后e句话
    :param artical:
    :param end_locate:
    :param result:
    :param b:
    :param e:
    :return:
    '''
    # 多减/加一个1，是因为-1/+1是正常的当前这句话
    b = -b - 1
    e += 1
    for pi in result:
        el = pi.locate[0]
        last_attr = list(pi.attr.keys())
        last_attr.sort(key=lambda x: x[0])
        la = last_attr[-1][0]

        tmp_end_locate = end_locate + [el, la]
        tmp_end_locate.sort()
        if tmp_end_locate.index(el) + b < 0:
            begin_pos = 0
        else:
            begin_pos = tmp_end_locate[tmp_end_locate.index(el) + b]

        if tmp_end_locate.index(la) + e >= len(tmp_end_locate):
            end_pos = len(tmp_end_locate) - 1
        else:
            end_pos = tmp_end_locate[tmp_end_locate.index(la) + e]

        pi.text = artical[begin_pos:end_pos]
    return result

def get_stock_infos():
    with AE().session_scope() as s:
        rs = s.query(Stock).all()
    stock_infos = []
    stock_names=[]
    for r in rs:
        if r.display_name not in stock_names:
            stock_infos.append((r.a_code, r.display_name, r.alias))
            stock_names.append(r.display_name)
    return stock_infos

def sentence_segment(artical):
    # 分句
    return end_pattern.split(artical)

def get_articals(artical_no=10):
    '''
    随机取artical_no个文章
    :param artical_no:
    :return:
    '''
    with AE().session_scope() as s:
        result = s.query(Article, ArticleRaw.text).filter(Article.article_id == ArticleRaw.article_id).limit(artical_no).all()
    articals = [r.text for r in result]
    return articals

def get_para_pattern():
    '''
    基于规则把文章分段
    规则1. 2个以上换行  对于大部分文章效果极差，取消这条规则
    规则2. 换行 会有一个列表符（可能无）+16个以内的空格（可能无）+中英数字
    规则3. 换行 16个以内的空格（可能无）+列表符
    规则4. 换行 空格（可能无）+左括号（可能无）+有序字符（A,B,C,1，2，3这种）+（右括号，
    停顿符，空格之一，可能多个的组合）
    规则5. 。+空格（可能无）+换行
    规则6. 。+3个以上空格
    :return: re.compile 所有规则, 所有规则文本
    '''
    space_no = '{0,16}'
    space_no2 = '{3,}'
    # rule1需要放在最后
    rule1 = f'{eol_sym}{eol_sym}+'
    # 不加space_no的情况会出现
    rule2 = f'{eol_sym}{list_sym}{blank_sym}{space_no}\w'
    rule3 = f'{eol_sym}{blank_sym}{space_no}{list_sym}'
    rule4 = f'{eol_sym}{blank_sym}{space_no}{left_sym}?{sequential_sym}{right_sym}'
    rule5 = f'\u3002{blank_sym}{space_no}{eol_sym}'
    rule6 = f'\u3002{blank_sym}{space_no2}'
    pattern_text=f'{rule4}|{rule2}|{rule3}|{rule5}|{rule6}'
    return re.compile(pattern_text), pattern_text

def check_mismatch(f,t):
    '''
    根据规则判断是否发生了错配
    规则为：如果前面f是个.结尾的数字，t是一个整数，且后面跟单位（单位列表后续更新），则认为发生了错配
    :param f:
    :param t:
    :return:
    '''
    # f和t是否可能是mismatch
    if mismatch_fore.match(f):
        if mismatch.match(t):
            return True
    return False

def del_principle(text):
    '''
    基于3个规则去除无效段落
    1. 段落前10个字符中有目录字样（这个规则很危险）
    2. 段落有大于3个连续的.
    3. 段落中中文少于10个。
    :param text:
    :return:
    '''
    if text[:10].find('目录')>0:
        return False
    if multi_dot.findall(text):
        return False
    if len(chinese_charactor.findall(text))<10:
        return False
    return True

def deal_mismatch(found, splited):
    '''
    根据规则判断是否发生了错配
    规则为：如果前面f是个.结尾的数字，t是一个整数，且后面跟单位（单位列表后续更新），则认为发生了错配
    :param found:
    :param splited:
    :return:
    '''
    new_list = []
    # 当发现下一行数据是mismatch时，把next_flag置为True
    next_flag = False
    for i in range(len(splited)):
        if next_flag:
            # next_flag 被标注成True了
            # 只能暂时假设没有连续的mismatch了。。。。
            next_flag = False
            continue
        if i == 0:
            if i + 1 != len(splited):
                if check_mismatch(found[i], splited[i + 1]):
                    next_flag = True
                    new_list.append(splited[i] + found[i] + splited[i + 1])
                else:
                    new_list.append(splited[i])
            else:
                new_list = splited
        else:
            if i + 1 != len(splited):
                if check_mismatch(found[i], splited[i + 1]):
                    next_flag = True
                    new_list.append(found[i - 1] + splited[i] + found[i] + splited[i + 1])
                else:
                    new_list.append(found[i - 1] + splited[i])
            else:
                new_list.append(found[i - 1] + splited[i])
    return new_list

def postprocess_split_para(splited):
    result=[mistart_pattern.sub('',eol_pattern.sub('',i)) for i in splited if del_principle(i)]
    return result

def decor_found(pre_decor):
    if all_char_sym.match(pre_decor):
        return ''
    return pre_decor[1:]

def split_artical(artical):
    '''
    基于规则对文章分段
    规则见get_para_pattern函数注释
    :param artical:
    :return:
    '''
    para_pattern,_=get_para_pattern()
    splited = para_pattern.split(artical)
    # split的时候会把找到的pattern去掉，后面是再给加回来
    found = para_pattern.findall(artical)
    # 下面这行是为了去掉\n，所有的pattern第一个字符必然是\n
    found = [decor_found(i) for i in found]
    the_list=deal_mismatch(found,splited)
    the_list=postprocess_split_para(the_list)
    return the_list

def get_property():
    '''
    :return:字典，property:property的关键字
    '''
    with AE().session_scope() as s:
        pk = s.query(PropertyKeyword).all()
        ppt = s.query(Property).all()
    pk_ = pd.DataFrame([[i.property_id, i.keyword] for i in pk])
    return {i.property:list(pk_[pk_[0]==i.property_id][1]) for i in ppt}

def get_property_patterns(ppt):
    '''
    返回keyword的pattern
    :param ppt: get_property的返回结果
    :return:dict->{ppt.key : keyword_patterns}
    '''
    ppt_patterns = {k: re.compile('|'.join(v)) for k, v in ppt.items()}
    return ppt_patterns

def find_keyword_through_para(para_ppt_dict, splited_artical, ppt_patterns):
    '''
    获得指定数据结构的dict，结构为{'para':[],'property':[]}，其中para的长度必须与property的长度一致
    :param para_ppt_dict:
    :param splited_artical:
    :param ppt_patterns:
    :return:
    '''
    assert len(para_ppt_dict['para'])==len(para_ppt_dict['property'])
    para=[]
    ppt=[]
    for ind, val in enumerate(splited_artical):
        para.append(val)
        ppt.append([])
        for k, v in ppt_patterns.items():
            if v.search(val):
                ppt[ind].append(k)
    para_ppt_dict['para'].extend(para)
    para_ppt_dict['property'].extend(ppt)

if __name__ == '__main__':
    # with open('/home/pb064/Documents/generated_text/7.29股市分析：散户主力同时.txt', 'r') as f:
    #     artical = f.read()
    articals = get_articals()
    properties = get_property()
    stock_infos=get_stock_infos()

    with AE().session_scope() as s:
        rs = s.query(Sector_Topic_Keywords).all()
    attributes = [r.keyword for r in rs]

    rec_no=0
    for artical in articals:
        print(rec_no)
        rec_no+=1
        artical=multi_break.sub('\n',artical)
        artical_name=punc_pattern.sub('',artical)[:15]
        end_locate = [m.span()[1] for m in end_pattern.finditer(artical)]
        result = get_attribute_locations(attributes, stock_infos, artical)
        result = get_text(artical, end_locate, result)
        with open('/home/pb064/Documents/generated_text/%s.txt'%artical_name,'w') as f:
            f.write(artical)
        with open('/home/pb064/Documents/generated_json/%s_attr.json' % artical_name, 'w') as f:
            # for i in result:
            #     f.write(str(i)+'\n')
            json.dump(result, f, cls=PI_Encoder, ensure_ascii=False)
    # result = get_attribute_locations(attributes, stock_names, artical)