import re

from tkdatabase.engine import ArticleEngine as AE
from tkdatabase.article import Stock, Sector_Topic_Keywords
from utils import cal_total_location, get_attr_belong_location


def get_stock_locations(stock_names, artical):
    '''
    在文章中找到所有命名实体的位置（关键词法），并进行实体对齐（固定位置法）
    '''
    all_stock_locations = dict()
    for stock in stock_names:
        # *ST是指连续亏损3年，ST是连续亏损2年
        totalName = stock[1].replace('*', '')
        totalNameLocation = [m.span() for m in re.finditer(totalName, artical)]
        if stock[2] is not None:
            aliasNameLocation = [m.span() for m in re.finditer(stock[2], artical)]
        else:
            aliasNameLocation = None
        kmp_result = cal_total_location(aliasNameLocation, totalNameLocation)
        if kmp_result:
            all_stock_locations[stock[0]] = kmp_result
    combine_stock_locations = []
    for i in all_stock_locations.values():
        combine_stock_locations += i
    return all_stock_locations, combine_stock_locations




def get_attribute_locations(attributes, stocks, artical):
    '''
    获得一篇文章中实体的属性
    :param attributes:属性名列表
    :param stocks:实体名列表
    :param artical:文章
    :return:dict(股票id:set(属性值))
    '''
    stock_locations, combine_stock_locations = get_stock_locations(stocks, artical)

    entity_attr_dict=dict()
    for attr in attributes:
        attr_location = [m.span() for m in re.finditer(attr, artical)]
        combine_tmp_locations = combine_stock_locations + attr_location
        combine_tmp_locations.sort(key=lambda x: x[0])
        for each_attr in attr_location:
            attr_belong_location = get_attr_belong_location(combine_tmp_locations, attr_location, each_attr)
            if attr_belong_location is None:
                # pass
                continue
            for key, val in stock_locations.items():
                if attr_belong_location in val:
                    the_key = key
                    if entity_attr_dict.get(the_key) is None:
                        entity_attr_dict[the_key] = set()
                    entity_attr_dict[the_key].add(attr)
    return entity_attr_dict

if __name__ == '__main__':
    with open('/home/pb064/Documents/测试语料/氨纶III.txt', 'r') as f:
        artical = f.read()
    with AE().session_scope() as s:
        rs = s.query(Stock).all()


    stock_infos = []
    stock_names=[]
    for r in rs:
        if r.display_name not in stock_names:
            stock_infos.append((r.a_code, r.display_name, r.alias))
            stock_names.append(r.display_name)

    # print(get_stock_locations(stock_names, artical))

    with AE().session_scope() as s:
        rs = s.query(Sector_Topic_Keywords).all()
    attributes = [r.keyword for r in rs]

    result=get_attribute_locations(attributes,stock_infos,artical)
    for key,val in result.items():
        print(key,' : ',val)