from collections import OrderedDict

import torch
from torch.nn import Module, Linear
from torch.optim import Adam
from torch.nn import functional as F

from pytorch_pretrained_bert import BertTokenizer, BertModel
# from pytorch_pretrained_bert.modeling import BertConfig

from utils import sentence_segment

# bert_config=BertConfig('/home/pb064/Ranglage/Models/FinBERT_L-12_H-768_A-12_pytorch/bert_config.json',)
def create_ffnn(input_size = 4608, output_size = 1, ffnn_size = None, ffnn_depth = 0, dropout=None):
    """ 创建前馈神经网络
    """
    if ffnn_size is None:
        ffnn_size=input_size//2
    if dropout is None:
        dropout=ffnn_depth*0.1
        if dropout>0.3:
            dropout=0.3

    current_size = input_size
    model_seq = OrderedDict()
    for i in range(ffnn_depth):
        model_seq['fc' + str(i)] = torch.nn.Linear(current_size, ffnn_size)
        model_seq['relu' + str(i)] = torch.nn.ReLU()
        model_seq['dropout' + str(i)] = torch.nn.Dropout(dropout)
        current_size = ffnn_size
    model_seq['output'] = torch.nn.Linear(current_size, output_size)
    return torch.nn.Sequential(model_seq)

class RelationAttentionLayer(Module):
    def __init__(self):
        super(RelationAttentionLayer, self).__init__()

    def forward(self,word):
        score = word@word.T
        score = F.softmax(score, dim=0)
        return (score.T@word).sum(0)

class RelationSelfAttentionLayer(Module):
    def __init__(self, hid_dim, n_heads):
        super().__init__()
        assert hid_dim % n_heads == 0

        self.hid_dim = hid_dim
        self.n_heads = n_heads
        self.head_dim = hid_dim // n_heads

        self.fc_q = Linear(hid_dim, hid_dim)
        self.fc_k = Linear(hid_dim, hid_dim)

        self.fc_o = Linear(hid_dim, hid_dim)

        self.scale = torch.sqrt(torch.FloatTensor([self.head_dim]))

    def forward(self, data, mask=None):
        # data = [batch size, character len, hid dim]
        batch_size = data.shape[0]

        Q = self.fc_q(data)
        K = self.fc_k(data)

        # Q = [batch size, data len, hid dim]
        # K = [batch size, data len, hid dim]

        Q = Q.view(batch_size, -1, self.n_heads, self.head_dim).permute(0, 2, 1, 3)
        K = K.view(batch_size, -1, self.n_heads, self.head_dim).permute(0, 2, 1, 3)

        # Q = [batch size, n heads, data len, head dim]
        # K = [batch size, n heads, data len, head dim]

        energy = torch.matmul(Q, K.permute(0, 1, 3, 2)) / self.scale

        # energy = [batch size, n heads, data len, data len]

        if mask is not None:
            energy = energy.masked_fill(mask == 0, -1e10)

        attention = torch.softmax(energy, dim=-1)
        # attention = [batch size, n heads, data len, data len]
        return attention

class Linear3D(Module):
    '''
    把三维矩阵linear成1维
    '''
    def __init__(self, in_features: int, out_features: int = 1, bias: bool = True):
        super(Linear3D, self).__init__()
        self.linear=Linear(in_features,out_features,bias)
    def forward(self,data):
        data=self.linear(data)
        return data.sum(0)



class RelationLayer(Module):
    def __init__(self, hidden_dim):
        super(RelationLayer, self).__init__()
        self.attention=RelationAttentionLayer()
        self.linear=create_ffnn()
        # self.bert=BertModel.from_pretrained('/home/pb064/Ranglage/Models/FinBERT_L-12_H-768_A-12_pytorch')
        # self.tokenizer=BertTokenizer.from_pretrained('/home/pb064/Ranglage/Models/FinBERT_L-12_H-768_A-12_pytorch')

    def forward(self,artical_matrix,position1,position2):
        # hidden_dim=bert.config.hidden_dim
        # 对于本模型，hidden_dim=768
        word1=artical_matrix[position1[0]:position1[1]]
        word2=artical_matrix[position2[0]:position2[1]]
        # word=[word length, hidden_dim]

        word1_att=self.attention(word1)
        word2_att=self.attention(word2)
        # word_att=[hidden_dim]

        word1_emb=torch.cat([word1[0],word1[-1],word1_att])
        word2_emb=torch.cat([word2[0],word2[-1],word2_att])
        # word_emb=[3 * hidden_dim]

        both_word=torch.cat((word1_emb,word2_emb))
        score=self.linear(both_word)
        return torch.tanh(score)

class RelationModel(Module):
    def __init__(self,dev):
        super(RelationModel, self).__init__()
        self.bert=BertModel.from_pretrained('/home/pb064/Ranglage/Models/FinBERT_L-12_H-768_A-12_pytorch')
        self.tokenizer=BertTokenizer.from_pretrained('/home/pb064/Ranglage/Models/FinBERT_L-12_H-768_A-12_pytorch')
        self.relation=RelationLayer(self.bert.config['hidden_size'])
        self.dev=dev

    def artical_tokenize(self,artical):
        max_position_embeddings=self.bert.config['max_position_embeddings']
        sentences = sentence_segment(artical)
        total_tokens = []
        for sentence in sentences:
            if not total_tokens:
                total_tokens.append("[CLS]")
            total_tokens.extend(self.tokenizer.tokenize(sentence))
            total_tokens.append("[SEP]")
        if len(total_tokens)>max_position_embeddings-1:
            total_tokens=total_tokens[:max_position_embeddings-1]
            total_tokens.append("[SEP]")
        ids=torch.tensor(self.tokenizer.convert_tokens_to_ids(total_tokens))
        segment_ids=torch.ones_like(ids)
        return ids,segment_ids

    def forward(self,artical,entity,attr):
        ids, segments_ids=self.artical_tokenize(artical)
        ids = ids.view(1, -1).to(self.dev)
        segments_ids = segments_ids.view(1, -1).to(self.dev)
        artical_matrix, _ = self.bert(ids,segments_ids,output_all_encoded_layers=False)
        return self.relation(artical_matrix,entity,attr)

def get_data():
    return torch.tensor([]),[],[],torch.tensor([0])


def train(epoch,dev,batch=64):
    rm=RelationModel(dev)
    rm.train()
    opt=Adam(rm.parameters())
    rm.to(dev)
    for e in range(epoch):
        artical, entities, attrs, real = get_data()
        for entity in entities:
            for attr in attrs:
                score=rm(artical,entity,attr)
                loss=F.cross_entropy(score,real)
                loss.backward()
        if 0==e%batch:
            opt.step()
            opt.zero_grad()