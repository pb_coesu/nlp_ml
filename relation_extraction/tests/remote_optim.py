import torch
from torch import nn
import torch.distributed as dist
from torch.distributed.distributed_c10d import _get_default_group,ReduceOp

broadcast_bucket_size = int(250 * 1024 * 1024)
backend='NCCL'
dist.init_process_group(backend, init_method='tcp://10.131.2.33:23456',rank=0, world_size=2)

model=nn.Sequential(
            nn.Linear(4, 1),
            nn.Sigmoid()
        )
dev = torch.device("cuda:0")
model.to(dev)

# 另一台机器运行这个
# dist.init_process_group(backend, init_method='tcp://10.131.2.33:23456',rank=1, world_size=2)
process_group=_get_default_group()

module_states = []
parameters_to_ignore=set()


def _find_common_rank(input_rank, rank_cond, device, process_group):
    # -1 indicates that this rank is not under consideration to be the
    # common_rank
    rank_to_use = torch.tensor(
        [input_rank if rank_cond else -1],
        device=device,
    )
    dist.all_reduce(rank_to_use, op=ReduceOp.MAX, group=process_group)
    if rank_to_use.item() == -1:
        raise ValueError(
            "BUG! Expected rank_cond to be true for at least one process."
        )
    return rank_to_use.item()


for name, param in model.state_dict().items():
    if name not in parameters_to_ignore:
        module_states.append(param)

if len(module_states) > 0:
    dist._broadcast_coalesced(
        process_group, module_states, broadcast_bucket_size, 0
    )