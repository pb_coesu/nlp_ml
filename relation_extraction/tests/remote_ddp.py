import torch
from torch import nn
import torch.distributed as dist
from torch.nn.parallel import DistributedDataParallel as DDP
import argparse

# 所有的这些参数都不需要设置
parser = argparse.ArgumentParser()
parser.add_argument("--local_rank", default=-1, type=int,
                    help="Local rank for distributed training.")
# 开启的进程数(注意不是线程),不用设置该参数，会根据nproc_per_node自动设置
parser.add_argument('--world_size', default=4, type=int,
                    help='number of distributed processes')
parser.add_argument('--dist_url', default='', help='url used to set up distributed training')
args = parser.parse_args()

torch.cuda.set_device(args.local_rank)
# 根据local rank设置显卡
dev=torch.device("cuda", args.local_rank)
backend='NCCL'
dist.init_process_group(backend)
# 小测试模型
model=nn.Sequential(
            nn.Linear(4, 1),
            nn.Sigmoid()
        )
model=model.to(dev)
model=DDP(model, device_ids=[args.local_rank], output_device=args.local_rank)
# 输出模型的参数，实验结果可以看到DDP(model)的过程中pytorch自动同步了参数
print(model.state_dict().items())

# 保存文件后，在一个节点运行下面命令。master_addr换成这个节点的IP，port用一个未被占用的端口即可
# python3 -m torch.distributed.launch --nproc_per_node=2 --nnodes=2 --node_rank=0 --master_addr="10.131.2.33" --master_port=23456 remote_ddp.py
# 在另一个节点运行下面命令，设定master_addr和port
# python3 -m torch.distributed.launch --nproc_per_node=2 --nnodes=2 --node_rank=1 --master_addr="10.131.2.33" --master_port=23456 remote_ddp.py