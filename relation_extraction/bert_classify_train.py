import torch
import pandas as pd
from pytorch_pretrained_bert import BertAdam
from bert_classify_model import BertForMultiLabelSequenceClassification
from torch.nn import BCELoss
from numpy import mean

train_batch_size=4
gradient_accumulation_steps=64
num_train_epochs=500

def get_data(count,batch_size,data,dev):
    data=data.iloc[count*batch_size:(count+1)*batch_size]
    x=data['para'].values
    y=list(data['ppt_vec'].values)
    y=torch.tensor(y).to(dev)
    return x, y

def gen_data(batch_size, data, dev):
    count=0
    while count*batch_size<len(data):
        yield get_data(count,batch_size,data,dev)
        count+=1

if __name__ == '__main__':
    dev = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    # data=pd.read_pickle('/home/pb064/Documents/generated_text/train.pkl')
    data = pd.read_pickle('/home/pb064/Documents/generated_text/train200.pkl')
    train_examples = len(data)

    model=BertForMultiLabelSequenceClassification.from_pretrained('/home/pb064/Ranglage/Models/FinBERT_L-12_H-768_A-12_pytorch',num_labels=4,dev=dev)
    loss_fn=BCELoss()
    num_train_steps = int(train_examples / train_batch_size / gradient_accumulation_steps) * num_train_epochs

    param_optimizer = list(model.named_parameters())
    no_decay = ['bias', 'gamma', 'beta']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)], 'weight_decay_rate': 0.01},
        {'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)], 'weight_decay_rate': 0.0}
    ]

    optimizer = BertAdam(optimizer_grouped_parameters,
                         lr=5e-5,
                         warmup=0.1,
                         t_total=num_train_steps)

    model.train().to(dev)
    loss_epoch = dict()

    for epoch in range(num_train_epochs):
        accumulation_steps=0
        accumulation_loss=[]
        loss_batch = []
        gen = gen_data(train_batch_size, data, dev)
        for x,y in gen:
            accumulation_steps+=1
            res=model(x)
            y=y.type_as(res)
            loss=loss_fn(res.view(-1, 1), y.view(-1, 1))
            accumulation_loss.append(loss.item())
            loss.backward()
            if accumulation_steps%gradient_accumulation_steps==0:
                optimizer.step()
                optimizer.zero_grad()
                mean_accu_loss=mean(accumulation_loss)
                print('accumulation batch loss', mean_accu_loss)
                loss_batch.append(mean_accu_loss)
                accumulation_loss=[]
        mean_epoch_loss=mean(loss_batch)
        loss_epoch[epoch] = loss_batch
        print(f'{epoch} epoch, epoch mean loss = {mean_epoch_loss}, progress % = {epoch*100/num_train_epochs}%')
    # 持久化工作
        if epoch%10==0:
            print('saved')
            torch.save(model,f'/home/pb064/Ranglage/Models/FinBERT_L-12_H-768_A-12_pytorch/small_fine_tuning_classify_{epoch}.pt')
    torch.save(loss_epoch,'/home/pb064/Ranglage/Models/FinBERT_L-12_H-768_A-12_pytorch/small_fine_tuning_classify_train_loss.pt')
