from collections import OrderedDict

import torch
from torch import nn
from torch.nn.utils.rnn import pad_sequence

from pytorch_pretrained_bert import BertTokenizer, BertModel
from pytorch_pretrained_bert.modeling import BertPreTrainedModel

from utils import sentence_segment

class BertForMultiLabelSequenceClassification(BertPreTrainedModel):
    def __init__(self, config, num_labels, dev):
        super(BertForMultiLabelSequenceClassification, self).__init__(config)
        self.num_labels = num_labels
        self.bert = BertModel(config)
        self.tokenizer = BertTokenizer.from_pretrained('/home/pb064/Ranglage/Models/FinBERT_L-12_H-768_A-12_pytorch')
        self.dropout = nn.Dropout(config.hidden_dropout_prob)
        self.classifier = nn.Sequential(
            nn.Linear(config.hidden_size, num_labels),
            nn.Sigmoid()
        )
        self.apply(self.init_bert_weights)
        self.dev = dev

    def artical_tokenize(self,artical):
        max_position_embeddings=self.bert.config.max_position_embeddings
        sentences = sentence_segment(artical)
        total_tokens = []
        for sentence in sentences:
            if not total_tokens:
                total_tokens.append("[CLS]")
            total_tokens.extend(self.tokenizer.tokenize(sentence))
            total_tokens.append("[SEP]")
        if len(total_tokens)>max_position_embeddings-1:
            total_tokens=total_tokens[:max_position_embeddings-1]
            total_tokens.append("[SEP]")
        ids = torch.tensor(self.tokenizer.convert_tokens_to_ids(total_tokens))
        return ids

    def forward(self, articals, token_type_ids=None, attention_mask=None):
        input_ids=[]
        for artical in articals:
            input_ids.append(self.artical_tokenize(artical))
        input_ids=pad_sequence(input_ids,batch_first=True).to(self.dev)
        _, pooled_output = self.bert(input_ids, token_type_ids, attention_mask, output_all_encoded_layers=False)
        pooled_output = self.dropout(pooled_output)
        logits = self.classifier(pooled_output)
        return logits
