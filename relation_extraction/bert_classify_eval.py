import torch
import pandas as pd
import torchmetrics
import fastHan



train_batch_size=4
gradient_accumulation_steps=64
num_train_epochs=500

def get_data(count,batch_size,data,dev):
    data=data.iloc[count*batch_size:(count+1)*batch_size]
    x=data['para'].values
    y=list(data['ppt_vec'].values)
    y=torch.tensor(y).to(dev)
    return x, y

def gen_data(batch_size, data, dev):
    count=0
    while count*batch_size<len(data):
        yield get_data(count,batch_size,data,dev)
        count+=1

if __name__ == '__main__':
    dev = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model=torch.load('/home/pb064/Ranglage/Models/FinBERT_L-12_H-768_A-12_pytorch/fine_tuning_classify_10.pt',map_location=dev)
    test_data=pd.read_pickle('/home/pb064/Documents/generated_text/test.pkl')
    model.eval()
    with torch.no_grad():
        gen = gen_data(train_batch_size, test_data, dev)
        total_x=[]
        total_y=[]
        for x,y in gen:
            res = model(x)
            y = y.type_as(res)
            total_x.append(res)
            total_y.append(y)