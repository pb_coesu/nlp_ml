import re
import sys

import pandas as pd

from tkdatabase.engine import ArticleEngine as AE
from tkdatabase.article import Stock, Sector_Topic_Keywords, Article, ArticleRaw, BlockSynonym, Property, PropertyKeyword, Block
from sklearn.utils import shuffle
from utils import split_artical, get_property, get_articals, find_keyword_through_para, get_property_patterns

def drop_empty_ppt(df):
    a = []
    def get_nonull_index(row):
        if len(row['property']) != 0:
            a.append(row.name)

    df.apply(get_nonull_index, axis=1)
    df = df.loc[a]
    return df

def train_test_split(df,frac = 0.8):
    df = shuffle(df)
    df = df.reset_index().drop(columns='index')
    split_no = int(len(df) * frac)
    train = df.loc[:split_no]
    test = df.loc[split_no:]
    return train,test

if __name__ == '__main__':
    ppt_patterns=get_property_patterns(get_property())
    articals = get_articals(30000)
    para_articals=[split_artical(i) for i in articals]
    para_ppt_dict={'para':[],'property':[]}
    for artical in para_articals:
        find_keyword_through_para(para_ppt_dict,artical,ppt_patterns)
    df=pd.DataFrame(para_ppt_dict)
    